package hitSegitiga

func LuasSegitiga(sisi1 float64, tinggi float64) float64 {
	luas := ((sisi1 * tinggi) / 2)
	return luas
}

func KelilingSegitiga(sisi1 float64, sisi2 float64, sisi3 float64) float64 {
	keliling := (sisi1 + sisi2 + sisi3)
	return keliling
}
