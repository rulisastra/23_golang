package main

// untuk membuat go bin, harus tambahin path bin di env windows TIAP PROJECT

import (
	hitLingkaran "23golang/lingkaran"
	hitPersegiPanjang "23golang/persegipanjang"
	hitSegitiga "23golang/segitiga"
	"fmt"
)

func main() {
	// segitiga
	luasSeg := hitSegitiga.LuasSegitiga(13, 17)
	fmt.Println("Luas Segitiga: ", luasSeg)

	kelSeg := hitSegitiga.KelilingSegitiga(13, 17, 17)
	fmt.Println("Keliling Segitiga: ", kelSeg)

	// persegipanjang
	luasPP := hitPersegiPanjang.LuasPersegiPanjang(20, 30)
	fmt.Println("Luas Persegi Panjang: ", luasPP)

	kelPP := hitPersegiPanjang.KelilingPersegiPanjang(20, 30)
	fmt.Println("Keliling Persegi Panjang: ", kelPP)

	// lingkaran
	luasLing := hitLingkaran.LuasLingkaran(30)
	fmt.Println("Luas Persegi Panjang: ", luasLing)

	kelLing := hitLingkaran.KelilingLingkaran(30)
	fmt.Println("Keliling Persegi Panjang: ", kelLing)
}
