package hitLingkaran

func LuasLingkaran(diameter float64) float64 {
	r := diameter / 2
	pi := 3.14
	luas := (pi * r * r)
	return luas
}

func KelilingLingkaran(diameter float64) float64 {
	keliling := (2 * 3.14 * (diameter / 2))
	return keliling
}
