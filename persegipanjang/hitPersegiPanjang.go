package hitLingkaran

func LuasPersegiPanjang(sisi1 float64, sisi2 float64) float64 {
	luas := (sisi1 * sisi2)
	return luas
}

func KelilingPersegiPanjang(sisi1 float64, sisi2 float64) float64 {
	keliling := ((sisi1 + sisi2) * 2)
	return keliling
}
